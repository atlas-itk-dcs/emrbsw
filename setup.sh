#!/bin/bash

export SW_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export CMTCONFIG=i686-raspbian-gcc63-opt
export SW_INST_PATH=${SW_PATH}/installed

export PATH=${SW_INST_PATH}/${CMTCONFIG}/bin:$PATH
export PATH=${SW_INST_PATH}/share/bin:$PATH
export LD_LIBRARY_PATH=${SW_INST_PATH}/${CMTCONFIG}/lib:$PATH
export PYTHONPATH=${SW_INST_PATH}/share/lib/python

export CMAKE_PREFIX_PATH=${SW_PATH}/..:${SW_PATH}/../tdaq/cmake_tdaq/cmake/
#export CMAKE_PROJECT_PATH=${SW_PATH}
export CMAKE_MODULE_PATH=/usr/share/cmake-3.7/Modules


