#!/bin/bash

# declare global varibale tdaq version
export TDAQ_VERSION=tdaq-11-02-00

# search for tdaq release base & cmake project path
TDAQPATHS=(/cvmfs/atlas.cern.ch/repo/sw/tdaq /sw/atlas )
for _cand in ${TDAQPATHS[@]}; do
    if [ -d ${_cand}/tdaq/$TDAQ_VERSION ]; then
	export TDAQ_RELEASE_BASE=${_cand}
	export CMAKE_PROJECT_PATH=${_cand}
        export TOOL_BASE=${_cand}/tools/x86_64-el9
        break
    fi
done

# search for lcg release base
LCGPATHS=(/cvmfs/sft.cern.ch/lcg/releases /sw/atlas/sw/lcg/releases )
for _cand in ${LCGPATHS[@]}; do
    if [ -d ${_cand} ]; then
	export LCG_RELEASE_BASE=${_cand}
	export LCG_INST_PATH=${_cand}
	export CONTRIB_BASE=${_cand}
	break
    fi
done

if [ -z ${TDAQ_RELEASE_BASE+x} ]; then echo "TDAQ release base not found"; return 0; fi

echo "---------"
echo "TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE}"
echo "LCG_RELEASE_BASE=${LCG_RELEASE_BASE}"
echo "CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}"
source $TDAQ_RELEASE_BASE/tdaq/$TDAQ_VERSION/installed/setup.sh
source $TDAQC_INST_PATH/share/cmake_tdaq/bin/setup.sh
echo "---------"

# declare python path
export PATH=$TDAQ_PYTHON_HOME/bin:$PATH

export EMRB_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export EMRB_INST_PATH=$EMRB_PATH/installed
export PATH=$EMRB_INST_PATH/$CMTCONFIG/bin:$PATH
export PATH=$EMRB_INST_PATH/share/bin:$PATH
export LD_LIBRARY_PATH=$EMRB_INST_PATH/$CMTCONFIG/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$EMRB_INST_PATH/$CMTCONFIG/lib:$PYTHONPATH
export PYTHONPATH=$EMRB_INST_PATH/share/lib/python:$PYTHONPATH
export PYTHONPATH=$HOME:.local/bin:$PYTHONPATH

export BOOST_ROOT=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/Boost/1.82.0/$CMTCONFIG 
export XERCESC_ROOT=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/XercesC/3.2.4/$CMTCONFIG

#setup Root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#missing tools
export LD_LIBRARY_PATH=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/pango/1.48.9/$CMTCONFIG/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/cairo/1.17.2/$CMTCONFIG/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/blas/0.3.20.openblas/$CMTCONFIG/lib:${LD_LIBRARY_PATH}
#export LD_LIBRARY_PATH=$LCG_INST_PATH/$TDAQ_LCG_RELEASE/harfbuzz/2.7.4/$CMTCONFIG/lib:${LD_LIBRARY_PATH}
