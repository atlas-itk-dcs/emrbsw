#!/usr/bin/env python
################################################################
#
# Configuration file for the reliancesw running on raspberry Pi
#
# Before this can be executed, we need:
# sudo useradd -m aid
# sudo usermod -aG sudo aid
# sudo usermod -aG users aid
# sudo usermod -aG gpio aid
# sudo usermod -aG i2c aid
# sudo cp /home/pi/.bashrc /home/aid/.
# sudo cp /home/pi/.profile /home/aid/.
# sudo chown -R aid.aid /home/aid
#
# Other stuff that might be interesting
#
# DNS servers in /etc/dhcpcd.conf
# static domain_name_servers=137.138.17.12 137.138.16.12
# sudo service dhcpcd restart
#
# NTP servers in /etc/systemd/timesyncd.conf
# [Time]
# NTP=137.138.16.13 137.138.17.134
#
# Restart services
# sudo systemctl stop systemd-timesyncd
# sudo systemctl daemon-reload
# sudo systemctl start systemd-timesyncd
#
# Carlos.Solans@cern.ch
# September 2021
################################################################

import os
import sys
import subprocess

def file_write(path, contents, force=False):
    if os.path.exists(path) and not force:
        print("File exists: %s" % path)
        return
    print("Write file: %s" % path)
    fw = open(path,'w')
    fw.write(contents)
    fw.close()
    pass

def file_append(path, lines, backup=True):
    if backup: os.system("cp %s %s.%s.bak" % (path,path,time.strftime("%Y%m%d-%H%M%S")))
    fr = open(path)
    fc = []
    for line in fr.readlines():
        line=line.strip()
        fc.append(line)
        pass
    for newline in lines:
        found=False
        for line in fc:
            if newline in line:
                found=True
                break
            pass
        if not found:
            fc.append(newline)
            pass
        pass
    fw = open(path,'w')
    for line in fc:
        fw.write(line+"\n")
        pass
    fw.close()
    pass

def file_replace(path, keyPair, backup=True):
    if backup: os.system("cp %s %s.%s.bak" % (path,path,time.strftime("%Y%m%d-%H%M%S")))
    print("Open: %s" % path)
    fr = open(path)
    fc = []
    for line in fr.readlines():
        line=line.strip()
        for key in keyPair:
            if line==key:
                print("Replace '%s' => '%s'" % (line,keyPair[key]))
                line=keyPair[key]
                break
            pass
        fc.append(line)
        pass
    fw = open(path,'w')
    for line in fc:
        fw.write(line+"\n")
        pass
    fw.close()
    pass

id_rsa="""
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAzCRWHVYT2VKqJeVIQaU0IGMuFxc4j5btxQpQQa1nUpHP/dbE
SqINZd9adJODLuFqN8K7ZdZbpT2KQp3+ASvkHh4P7zuI+m4L8LFPmKD15xHMi+rV
fl/UFssldYXSobgHDIclYZSf+1oQU4JXf47uJVsEa73sUge2wtbN6NiRjjuxX8EA
0x1RR4IXCWvlDzl980+Ga7821gwXNYppRHRCSxf4LHOILxrvYHoGm4whXn12L/C2
M+kwrA+rIyHG5vj15H2c3QlbT+mjLP4LzyV3VLxjYroTLfdIRCKc0ArR+h5jZtOM
dWG23S1ljmbd8zCEYXNBwVpQ9Y3OAFPlhGqnahVgZUzniC6LCDh168mrjFHFFflZ
fTiX8WibWxO6ntnETu4qaiSiwkHE7nUtPV1MhQJoqlew0dbh/KSQKtsusf898stT
Kp88vcFpqjO1earfkgbdl7S9a/oPtKWdSoNmp781p8mMhq0rOO1qa3WN8O0SQczY
qOBU0BC+wxEHjFnWzhIbI0iWLXur7AIODxPUNwSlCSdQa7L74UzNtuLW2BPaIOPX
tYcjqcAMKqmLjqiaWEEgXdvUkgf4TuJGicQ/QRCVEkiO9i8z35x9zlb73iN+rZUw
bCZ/aKjk5k+hk/Zu8aZtBe8lWxvz68zI2kWpe1fMLv9+Ba/6c3zbVC14ymcCASMC
ggIBAMZPLxUvC/evY3VFIZ7a/2/rUVhCcXWLTWBTKWRfTm13tBsoZulqOOaedSDJ
z+Rl8ieJ999FQxWMPSrFXSW1m8V8Sf5XF1msuyRUeTUupbvlZ53cz2TSHnyK0+6B
/9BMXp50p/hkfidezgf69emgyhWo37ke5ZGD1hxbo3R6nAaDIVW0LK/TVkV3DxB3
cMz2BVLCSBDlovvROx4RbZL0mCuw4nRS+VJUrgXy4djuhtDRpgKvUelI7XPx9rRp
9GtYP1L9rlMQZ1TxlzMFao62rm+Sbyy0wiynEwBcI03BXkNt+ilfykYkaH8WKCPu
5jyyyb3Aa8MkiSkNaQeh9OcIhV4AuLoWIBsAxSL/WbNaiNEglzu5T5Kbhz16KPPk
OtLm78IeXugGYvoowzGq9+A0tpdWK4y45nFOIVUXZHdQyrVDhJCuiZV58MPLkW47
v98AZYZpXaneZyUV+CXeu/BlhoQHG0K+bG230tDF4OEecgr7Q+02IyhB0cS7b/yO
uARfL5oIzgc8TTTpUpZ+ZihQw8Fsn5lFI0lQwKh/2rz0bcRMnyZkRe/2CVJaJD9n
RRNjuJw6hEAMfitINi7BH9Usr//KK3WrsjNGXU85BKOjq4VH1N4gnysCRUZ1Tnkl
qiX1llILz5LfQr9nhXkJMIGIrEAl30g9hX19mQBF/AJ/2FIrAoIBAQDqGvTk7uAm
ttp8EkJA0Az6jad7eukiWLI7EXJyaVxAPy9R0HqsKZtLSqbNk8p9ryFfeq3hBaec
PYQo35VnkGVTF4OmApERHOpiolx0XzbLj42X4xFikg0mln2QeF6yK+CxmicS+Apd
P3NOT5ibmoAhyTKivAH0RbvBLeETbr1SzEKUeyXG3DIEJFFQoRpNLnKZ5UGZsK0P
ZXlsQJsorKfmxMtCOiJL8wmRvyv6eA6Ih7mYh7VtXbYRlADGAOuaiBP8+aC8lRwy
RYJTEyf2WHKVBO5Ac1Sm1RqDw1Mhms2YC5Er5cJooY7vQXEdMb9wAwOOAq5Cl4ui
FaG2G/HW8qdJAoIBAQDfO/wkBMh3+i9bLPhvbMYuI0eZdhPBcEOoQaJFbw8kQwT1
jtNx5hZjGsGz1oSLO8fZSK9d+i4P3ILFfd5kNwqMSyWAs+kYIHM6Zk02iTeqMszk
ZWaD0EAtzJegzN3DEjAVWcsQrGW4VoVL4IGzFKG5kjGQf/17II2OnZo9gQbDLF3d
+RQtECbhspZcfEO+zTICddbzoRFI7jLkdzsY5l6S6flnkdDrF/iVRcx5eZHQAcGZ
iP0WT1UN8EsoSn70Ly1LBUs9HMNSnV6OuLKhkPjxtTUF+x/uRN/3qyWHQQi0ZsC5
qg43Rp26mgWemeDI9Sax3B/I3Q92A4268KsPJ3QvAoIBAQDjaqTBIpCaowAgwUes
rNlobFmN5Rz8yzDLqoxvJIWAPWEq58ef7edQdGeF4AaIuMFVb9wca+SmZ6T0gWyQ
faRCFteolMdvtbB9EsA2iGEdhCMl1UQWnIHNtsMeoNEE2iq7KAizV1qGeCbeW/NV
VEH071XCqAHl+pkiD0+sejRBzb0FRG3XHwwEBgXZTArr4/pMWwyGq6DNIL8Ys8ns
/36IZ2Zdl48H82hhsmU8dKBnX0aULA9UTGe5TfIfbpu6sBNqxpwk6J8MQ4XqTR+B
l7/D9ilF7GDclIAoOhY95tZZL87aL6bpTHw44HyCwpy19EyYlOPLqSiO0y9gcuru
EEq7AoIBAQDY2y9zclxXSs7q2zp63rHjrT49TiHR2sVhntgmMV8qij9U8SUs0OKM
KJ7pNsnmV1RsrQIgxyVufnBoE99aCZU30/iLp3S4WgmJMC2/8wLf2ZPWjmOV/YAs
fZqcNLorNj1Wkb3y8J1T/Eb5QH3+a9BN3nlKivY9GE7/kckIi/fwzANp6qXiofni
kDpLNuK5WZ5LiGppAt2XTcsJ0ujznfV4xgg4uYke5BYNWb9RbsgpJkcKLU2gpNZI
Df/d/zl/fklBkB00DVAHHIe91/38CSxYhCThPRe0Qugya74OXG7bIf0MIYonEWYF
ujFYPbXKh78wb29VaQe0eHsNXtIOuJy/AoIBAQCoxjE24ONAkGfHkZtG+s7VANqM
dbCG+XMAxo0OEnP77ZoMW7vnIbGBcOTj1EM4GBQFNlaF47RQjr0YcSsrJjLQnEoH
+/GKozH1lYqwY+1fgBoULucBCnfB0jOHDLeadKLjbbFX74A6/4e18JDwzfNPE3xS
4t+zhKVoH5XLhRrnh6KF5+7xg9CWa8TZH8aonkbDV5nhhUYdxa1RxpiugkJOXZfN
4jJD521DuWpccX+5pKfNMCDHO7VpZmzjTdVEYn6t/0J4tsSPIX+7fsItKJ/E+Ll4
2IF024hDgqynue18WkakopEkTfvAKO7AmCawX9Qz4mO5QbWWq2jqO1YEwcKJ
-----END RSA PRIVATE KEY-----
"""

id_rsa_pub="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAzCRWHVYT2VKqJeVIQaU0IGMuFxc4j5btxQpQQa1nUpHP/dbESqINZd9adJODLuFqN8K7ZdZbpT2KQp3+ASvkHh4P7zuI+m4L8LFPmKD15xHMi+rVfl/UFssldYXSobgHDIclYZSf+1oQU4JXf47uJVsEa73sUge2wtbN6NiRjjuxX8EA0x1RR4IXCWvlDzl980+Ga7821gwXNYppRHRCSxf4LHOILxrvYHoGm4whXn12L/C2M+kwrA+rIyHG5vj15H2c3QlbT+mjLP4LzyV3VLxjYroTLfdIRCKc0ArR+h5jZtOMdWG23S1ljmbd8zCEYXNBwVpQ9Y3OAFPlhGqnahVgZUzniC6LCDh168mrjFHFFflZfTiX8WibWxO6ntnETu4qaiSiwkHE7nUtPV1MhQJoqlew0dbh/KSQKtsusf898stTKp88vcFpqjO1earfkgbdl7S9a/oPtKWdSoNmp781p8mMhq0rOO1qa3WN8O0SQczYqOBU0BC+wxEHjFnWzhIbI0iWLXur7AIODxPUNwSlCSdQa7L74UzNtuLW2BPaIOPXtYcjqcAMKqmLjqiaWEEgXdvUkgf4TuJGicQ/QRCVEkiO9i8z35x9zlb73iN+rZUwbCZ/aKjk5k+hk/Zu8aZtBe8lWxvz68zI2kWpe1fMLv9+Ba/6c3zbVC14ymc= adecmos@cern.ch"

packages={"OS":["python-lxml","cmake","libboost1.67","libxml2-utils",
                "openjdk-8-jdk","xsdcxx","libxerces-c-dev","astyle",
		"libssl-dev","kdiff3","python-colorama","python-jinja2",
                "screen","dnsutils","libgit2-dev","python-pygit2","emacs"],
          "atlas-itk-dcs":["emrb","emrb-opc-client","emrb-opc-server"],
	  }



print("Install RSA private key")
os.system("mkdir -p %s/.ssh" % os.environ["HOME"])
fw=open("%s/.ssh/id_rsa"%os.environ["HOME"],"w")
fw.write(id_rsa)
fw.close()
os.system("chmod 0600 %s/.ssh/id_rsa" %os.environ["HOME"])

print("Install RSA public key")
fw=open("%s/.ssh/id_rsa.pub"%os.environ["HOME"],"w")
fw.write(id_rsa_pub)
fw.close()

print("Install authorized keys")
found=False
os.system("touch %s/.ssh/authorized_keys" % os.environ["HOME"])
for line in open("%s/.ssh/authorized_keys"%os.environ["HOME"]).readlines():
    if id_rsa_pub in line: found=True
    pass
if not found:
    fw=open("%s/.ssh/authorized_keys"%os.environ["HOME"],"a")
    fw.write(id_rsa_pub)
    fw.close()
    pass

print("Install packages...")
cmd="sudo apt update"
print(cmd)
os.system(cmd);
cmd="sudo apt update --allow-releaseinfo-change"
print(cmd)
os.system(cmd);
cmd="sudo apt upgrade"
print(cmd)
os.system(cmd);
cmd="sudo apt-get -y install "
for p in packages["OS"]:
	cmd+= " "+p
	pass
print(cmd)
os.system(cmd);


print("Checkout reliancesw...")
os.system("git clone ssh://git@gitlab.cern.ch:7999/wald/reliancesw.git %s/reliance" % os.environ["HOME"])
os.chdir("%s/reliancesw" % os.environ["HOME"])

print("Install packages...")
for pkg in packages["wald"]:
	dst=pkg if "-" not in pkg else pkg.split("-")[1]
	print ("Checking out wald/%s" % pkg)
	os.system("git clone ssh://git@gitlab.cern.ch:7999/wald/%s.git %s/reliancesw/%s" % (pkg,os.environ["HOME"],dst))
	pass

print ("compile reliancesw...")
os.system("mkdir -p %s/reliancesw/build" % os.environ["HOME"])
os.chdir("%s/reliancesw/build" % os.environ["HOME"])
os.system("source %s/reliancesw/setup.sh && cmake .. && make -j install" % os.environ["HOME"])

print ("Change hostname from DNS")
os.system("sudo python %s/reliancesw/share/change_hostname_from_dns.py")

print ("Have a nice day")


