#!/usr/bin/env python

import os
import sys
import socket
import subprocess

print("find out the host name")
ip=subprocess.check_output('hostname -I',shell=True).decode('utf-8').strip().split()[0]
hn=socket.gethostbyaddr(ip)[0].split(',')[0].split('.')[0]
print("hostname: %s" %hn)

print("changing the system settings")
fw=open('/etc/hostname','w')
fw.write(hn+'\n')
fw.close()
os.system("hostname %s"%hn)

print("done. Have a nice day")

