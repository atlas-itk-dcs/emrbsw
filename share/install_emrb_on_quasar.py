#!/usr/bin/env python

import os
import sys
import time
import argparse

def file_replace(path, keyPair, backup=True):
    if backup: os.system("cp %s %s.%s.bak" % (path,path,time.strftime("%Y%m%d-%H%M%S")))
    print ("Open: %s" % path)
    fr = open(path)
    fc = []
    for line in fr.readlines():
        line=line.strip()
        for key in keyPair:
            if line==key:
                print ("Replace '%s' => '%s'" % (line,keyPair[key]))
                line=keyPair[key]
                break
            pass
        fc.append(line)
        pass
    fw = open(path,'w')
    for line in fc:
        fw.write(line+"\n")
        pass
    fw.close()
    pass


parser = argparse.ArgumentParser()
parser.add_argument("emrbsw_dir",help="emrbsw path")
parser.add_argument("opcua_dir",help="opcua path")
args = parser.parse_args()

files={"emrb":["emrb/Emrb.h",
               "src/Emrb.cpp"],
       }


print("Check OPC-UA project exists")
if not os.path.exists(args.opcua_dir):
    print ("Project does not exist: %s" % args.opcua_dir)
    sys.exit()
    pass

print("Check source files exist")
for pkg in files:
    for f in files[pkg]:
        fp="%s/%s/%s" % (args.emrbsw_dir,pkg,f)
        if not os.path.exists(fp):
            print ("File does not exist: %s" % f)
            sys.exit()
            pass
        pass
    pass

print("Copy files to OPC-UA project")
for pkg in files:
    for f in files[pkg]:
        src="%s/%s/%s" % (args.emrbsw_dir,pkg,f)
        dst="%s/%s/%s" % (args.opcua_dir,pkg,f)
        if not os.path.exists(os.path.dirname(dst)):
            os.system("mkdir -p %s" % os.path.dirname(dst))
            pass
        os.system("cp -v %s %s" % (src,dst))
        pass
    pass

print("Create CMakeLists.txt files")
for pkg in files:
    print("File: %s/%s/CMakeLists.txt" % (args.opcua_dir,pkg))
    ttt ="#This file was automatically generated\n"
    ttt+="#on %s\n" % time.strftime("%d/%m/%Y at %H:%M:%S")
    ttt+="\n"
    ttt+="add_library(%s OBJECT \n" % pkg
    for f in files[pkg]:
        if not "src" in f: continue
        ttt+="            %s\n" % f
        pass
    ttt+="           )\n"
    ttt+="\n"
    ttt+="target_include_directories(%s PUBLIC .)\n" % pkg
    fw=open("%s/%s/CMakeLists.txt" % (args.opcua_dir,pkg),"w+")
    fw.write(ttt)
    fw.close()
    pass

print("Update the OPC-UA project settings")
file_replace("%s/ProjectSettings.cmake" % args.opcua_dir,
             {"set(CUSTOM_SERVER_MODULES )":"set(CUSTOM_SERVER_MODULES %s)" % " ".join(files.keys()),
              "set(SERVER_INCLUDE_DIRECTORIES )":"set(SERVER_INCLUDE_DIRECTORIES %s)" % " ".join(files.keys())}
             )

print("Have a nice day")
