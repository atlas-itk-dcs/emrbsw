#!/usr/bin/env python

import os
import sys
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("--version",help="tdaq version. default: 11.2.0",default="11.2.0")
parser.add_argument("--binarytag",help="CMTCONFIG tag. default: i686-raspbian-gcc63-opt",default="i686-raspbian-gcc63-opt")
parser.add_argument("--base",default=os.getenv('HOME'))
args=parser.parse_args()

print("Install local cmake")
print("- tdaq version: %s" % args.version)
print("- binary tag: %s" % args.binarytag)
print("- base path: %s" % args.base)

ver=args.version.split(".")
tag="tdaq-%02i-%02i-%02i" % (int(ver[0]),int(ver[1]),int(ver[2]))

print("Create tdaq folder: %s/tdaq" % args.base)
os.system("mkdir -p %s/tdaq" % args.base)

print("Checkout the cmake_tdaq package from git")
os.system("git clone https://gitlab.cern.ch/atlas-tdaq-software/cmake_tdaq.git %s/tdaq/cmake_tdaq" % args.base)
os.system("cd %s/tdaq/cmake_tdaq; git checkout c02c3e48f4e50852bee016a9502d89bb1863eec3" % args.base)

print("Create the installed folder: %s/tdaq/%s/installed/%s" % (args.base,tag,args.binarytag))
os.system("mkdir -p %s/tdaq/%s/installed/%s" % (args.base,tag,args.binarytag))

files={}

files["tdaqConfig.cmake"]="""
find_package(Boost)
find_package(PythonInterp)
find_package(PythonLibs)
SET(BOOST_VERSION 0.0)
SET(TDAQ_VERSION "%s")
SET(ENV{TDAQ_VERSION} "%s")
"""%(tag,tag)

files["tdaqConfigVersion.cmake"]="""
set(PACKAGE_VERSION "%s")

if(PACKAGE_FIND_VERSION STREQUAL PACKAGE_VERSION)
  set(PACKAGE_VERSION_EXACT TRUE)
endif()
"""%args.version

for f in files:
    print("Write file: %s/tdaq/%s/installed/%s/%s" % (args.base,tag,args.binarytag,f))
    fw=open("%s/tdaq/%s/installed/%s/%s" % (args.base,tag,args.binarytag,f),"w+")
    fw.write(files[f])
    fw.close()
    pass

print("Have a nice day")
