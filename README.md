# EMRBSW

This is the release package to help build the Environmental Monitoring Readout Board software.

## How to build the reliance sw

0. Checkout the emrb package

```
git clone https://gitlab.cern.ch/atlas-itk-dcs/emrbsw.git
```

1. Change into the emrbsw directory

```
cd emrbsw
```

2. Checkout the release packages

```
git clone https://gitlab.cern.ch/atlas-itk-dcs/emrb.git
git clone https://gitlab.cern.ch/atlas-itk-dcs/emrb-opc-server.git
git clone https://gitlab.cern.ch/atlas-itk-dcs/emrb-opc-client.git
```

3. Install a local copy of the TDAQ CMake policy

```
python share/install_local_cmake.py 
```

4.a Setup the environment for the Raspberry Pi

```
source setup.sh
```

4.b. Alternatively source the environment for Alma9

```
source setup-lxplus.sh
```

4. Create the build directory

```
mkdir build
```

5. Change into the build directory

```
cd build
```

6. Configure the release

```
cmake ..
```

7. Build the release

```
make -j install
```

8. Please note that the emrb-opc-server project has to be compiled separatedly

## Installing and compiling quasar for the first time

1. Update the OS

```
sudo apt-get update
```

2. Install missing tools

```
sudo apt-get -y install \
python-lxml \
cmake \
libboost1.74 \
libxml2-utils \
openjdk-17-jdk \
xsdcxx \
libxerces-c-dev \
astyle \
libssl-dev \
kdiff3 \
python3-colorama \
python3-jinja2 \
python3-pygit2 \
screen \
dnsutils \
libgit2-dev 
```

3. Checkout quasar

```
git clone --recursive https://github.com/quasar-team/quasar -b v1.6.1
```
